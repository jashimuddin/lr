<?php

require_once 'inc/header.php';
require_once 'lib/User.php';
Session::checkLogin();

$user = new User();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])){
	 $login = $user->userlogin($_POST);
}

?>

<br>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">


			<div class="panel panel-success">
				<div class="panel-heading">
					<h2 class="panel-title"> Login with your credentials</h2>
				</div>
				<div class="panel-body">
					<?php 
					if (isset($login)){ 
						echo $login;
					}
					?>
					<form action="login.php" method="POST">

						<div class="form-group">
							<label for="">Email:</label>
							<input type="text" class="form-control" name="email">
						</div>
						<div class="form-group">
							<label for="">Password:</label>
							<input type="password" class="form-control" name="password">
						</div>

                        <button class="btn btn-primary pull-right" type="submit" name="login"> Login </button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>  <!-- // login container ended here -->




<br> <br>
<br>
<?php  require_once 'inc/footer.php'; ?>
