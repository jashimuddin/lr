<?php

class PDOAdapter implements AdapterInterface
{
	protected $connection;
	protected $result;


	/**
	 * PDOAdapter constructor.
	 *
	 * @param $host
	 * @param $username
	 * @param $password
	 * @param $dbname
	 */
	public function __construct($host, $username, $password, $dbname) {

		try {
			$this->connection = new PDO('mysql:host=$host;dbname=$dbname', $username, $password);

			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		} catch (PDOException $e){

			echo "Failed to connect database".$e->getMessage();
		}
	} // End of constructor


	/**
	 * @param $sql .
	 *
	 * @return $this
	 */

	public function query( $sql ) {

		$query = $this->result = $this->connection->prepare($sql);

		$stmt = $query->execute();

		if ($query->columnCount() === 0 ) {
			$this->result = $stmt;
		} else {
			$this->result = $query;
		}
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function result()
	{
		if (gettype($this->result) === '1'){
			return $this->result;
		} else {
			$data = [];

			while ($row = $this->result->fetch(PDO::FETCH_OBJ)){
				$data[] = $row;
			}
			return $data;
		}
	} // end of result method


} // End of Class declaration