<?php

interface AdapterInterface

{
	/**
	 * @param $sql
	 *
	 * @return mixed
	 */
	public function query($sql);

	/**
	 * @return mixed
	 */
	public function result();
}