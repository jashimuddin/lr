<?php

class Database
{

	protected $adapter;

	/**
	 * Database constructor.
	 *
	 * @param AdapterInterface $adapter
	 */
	public function __construct (AdapterInterface $adapter)
	{
		 $this->adapter = $adapter;

	} // End of Construct method


	/**
	 * @param $sql
	 *
	 * @return mixed
	 */
	public function query($sql)
	{
		return $this->adapter->query($sql);
	} // end of query


	/**
	 * @return mixed
	 */
	public function result() {
		return $this->adapter->result();
	}


} // End of Database class
