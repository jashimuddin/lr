<?php
/**
 * User Class 
 */

require_once 'Database.php';
require_once 'AdapterInterface.php';
require_once 'PDOAdapter.php';

class User
{

	private $db;

	
	public function __construct(){
		$this->db = new Database(new PDOAdapter('localhost', 'root', '4833', 'db_lr'));
	}

	/**
	 * @param $data
	 *
	 * @return string
	 */
	public function register( $data ) {

		$name = $data['name'];
		$username = $data['username'];
		$email = $data['email'];
		$password = md5($data['password']);
		$chk_mail = $this->emailCheck($email);

		if ($name == "" || $username == "" || $email =="" || $password == ""){
			$msg = "<div class='alert alert-danger'> Field must not be empty</div>";
			return $msg;
		}

		if (strlen($username) < 3 ){
			$msg = "<div class='alert alert-danger'> username is too short</div>";
			return $msg;
		} elseif (preg_match('/[^a-z0-9_-]+/i', $username)){
			$msg = "<div class='alert alert-danger'> username must contain alphanumerical, underscore and dashes</div>";
			return $msg;
		}


		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false){
			$msg = "<div class='alert alert-danger'> Invalid email address !</div>";
			return $msg;
		}
		if ($chk_mail == true){
			$msg = "<div class='alert alert-danger'> Email address already taken !</div>";
			return $msg;
		}

		// Insert Data

		$sql = "INSERT INTO tbl_user (name, username, email, password) VALUES (:name, :username, :email, :password)";
		$stmt = $this->db->connection->query($sql);
		$stmt->bindValue(':name', $name);
		$stmt->bindValue(':username', $username);
		$stmt->bindValue(':email', $email);
		$stmt->bindValue(':password', $password);

		$result = $stmt->execute();

		if ($result){
			$msg = "<div class='alert alert-success'> <b>Success!</b> User added successfully.</div>";
			return $msg;
		} else{
			$msg = "<div class='alert alert-danger'> <b>Error!</b> User Not added successfully.</div>";
			return $msg;
		}

	} // end of usrRegi method


	/**
	 * @param $email
	 *
	 * @return bool
	 */
	public function emailCheck($email) {
		$sql = "SELECT * FROM tbl_user WHERE  email = :email";
		$stmt = $this->db->connection->prepare($sql);
		$stmt->bindValue(':email' , $email);
		$stmt->execute();
		if ($stmt->rowCount() > 0){
			return true;
		} else {
			return false;
		}
	} // end


	/**
	 * @param $email
	 * @param $password
	 *
	 * @return mixed
	 */
	public function getLoginUser($email, $password) {
		$sql = "SELECT * FROM tbl_user WHERE  email = :email AND password = :password LIMIT 1 ";
		$stmt = $this->db->connection->prepare($sql);
		$stmt->bindValue(':email' , $email);
		$stmt->bindValue(':password' , $password);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_OBJ);
		return $result;

	}


	/**
	 * @param $data
	 *
	 * @return string
	 */
	public function userlogin($data) {
		$email = $data['email'];
		$password = md5($data['password']);
		$chk_mail = $this->emailCheck($email);

		if ($email == '' || $password == '') {
			$msg = "<div class='alert alert-danger'><b>WARNING:</b> Field must not be empty !! </div>";
			return $msg;
		}


		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false){
			$msg = "<div class='alert alert-danger'> Invalid email address !</div>";
			return $msg;
		}

		if ($chk_mail == false) {
			$msg = "<div class='alert alert-danger'>Email not exists!</div>";
			return $msg;
		}

		$result = $this->getLoginUser($email, $password);
		

		// var_dump($result);
		if ($result) {
			Session::init();
			Session::set('login', true);
			Session::set('id', $result->id);
			Session::set('name', $result->name);
			Session::set('username', $result->username);
			Session::set('userid', $result->id);
			Session::set('loginmsg', "<div class='alert alert-success'> You are logged in successfully !</div>");
			header('Location:index.php');
		} else {
			$msg = "<div class='alert alert-danger'> Data not found </div>";
			return $msg;
		}




	} // end of login

	/**
	 * @return mixed
	 */
	public function index(){
		$sql = " SELECT * FROM tbl_user ORDER By id DESC";
		$stmt = $this->db->connection->query($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $result;
	} // End of Index 

	/**
	 * @param $id
	 *
	 * @return mixed
	 */
	public function getUserByID($id){

		$sql = "SELECT * FROM tbl_user WHERE id = :id";
		$stmt = $this->db->connection->prepare($sql);
		$stmt->bindValue(':id' , $id);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_OBJ);
		return $result;
	}

	/**
	 * @param $id
	 * @param $data
	 *
	 * @return string
	 */
	public function updateUserData($id, $data){
		$name		= $data['name'];
		$username	= $data['username'];
		$email		= $data['email'];
		// Validate data 
		if ($name == "" || $username == "" || $email ==""){
			$msg = "<div class='alert alert-danger'> Field must not be empty</div>";
			return $msg;
		}
		// Validate username 
		if (strlen($username) < 3 ){
			$msg = "<div class='alert alert-danger'> username is too short</div>";
			return $msg;
		} elseif (preg_match('/[^a-z0-9_-]+/i', $username)){
			$msg = "<div class='alert alert-danger'> username must contain alphanumerical, underscore and dashes</div>";
			return $msg;
		}
		// Validate Email 
		if (filter_var($email, FILTER_VALIDATE_EMAIL) === false){
			$msg = "<div class='alert alert-danger'> Invalid email address !</div>";
			return $msg;
		}
		// Insert Data
		$sql = "UPDATE tbl_user SET 
				name	 = :name, 
				username = :username, 
				email	 = :email 
				WHERE id = :id";

		$stmt = $this->db->connection->prepare($sql);
		$stmt->bindValue(':name', $name);
		$stmt->bindValue(':username', $username);
		$stmt->bindValue(':email', $email);
		$stmt->bindValue(':id', $id);
		$result = $stmt->execute();

		if ($result){
			$msg = "<div class='alert alert-success'> <b>Success!</b> User data updated successfully.</div>";
			return $msg;
		} else{
			$msg = "<div class='alert alert-danger'> <b>Error!</b> User data not updated due to error.</div>";
			return $msg;
		}
	}   // End of update user data

	// check for Password to match with new password 
	/**
	 * @param $id
	 * @param $old_pass
	 *
	 * @return bool
	 */
	public function checkPassword($id, $old_pass){
		$password = md5($old_pass);
		$sql = "SELECT password FROM tbl_user WHERE id = :id AND password = :password";
		$stmt = $this->db->connection->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->bindValue(':password', $password);
		$stmt->execute();
		if ($stmt->rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} // end of check password

	/**
	 * @param $id
	 * @param $data
	 *
	 * @return string
	 */
	public function	 updatePass($id, $data){

		$old_pass = $data['old_pass'];
		$new_pass = $data['new_pass'];
		$chk_pass = $this->checkPassword( $id, $old_pass);
		if($old_pass == "" || $new_pass == ""){
			$msg = "<div class='alert alert-danger'> <b>Error!</b> Field must not be empty.</div>";
			return $msg;
		}
		
		if($chk_pass == false){
			$msg = "<div class='alert alert-danger'> <b>Error!</b> Password not found </div>";
			return $msg;
		}
		
		$password = md5($new_pass);
		$sql = "UPDATE tbl_user SET 
				password = :password 
				WHERE id = :id";

		$stmt = $this->db->connection->prepare($sql);
		$stmt->bindValue(':id', $id);
		$stmt->bindValue(':password', $password);
		
		$result = $stmt->execute();

		if ($result) {
			Session::set('msg', "<div class='alert alert-success'> Password updated successfully !</div>");
			header('Location:profile.php');
		} else {
			$msg = "<div class='alert alert-danger'> Password not updated </div>";
			return $msg;
		}


	}
	
}
