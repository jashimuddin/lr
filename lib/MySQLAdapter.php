<?php

class MySQLAdapter implements AdapterInterface {
	protected $connection;

	protected $result;

	public function __construct( $host, $username, $password, $dbname ) {
		$this->connection = new mysqli( $host, $username, $password, $dbname );
	}

	public function query( $sql ) {
		$this->result = $this->connection->query( $sql );

		return $this;
	}

	public function result() {
		if ( gettype( $this->result ) === 'boolean' ) {
			return $this->result;
		} elseif ( $this->result->num_rows > 0 ) {
			$result = [];

			while ( $row = $this->result->fetch_assoc() ) {
				$result[] = $row;
			}

			return $result;
		} else {
			return [];
		}
	}
}