<?php 
// require_once 'lib/Session.php';
require_once 'lib/User.php';
require_once 'inc/header.php'; 
Session::checkSession();

if(isset($_GET['id'])){
	$id = $_GET['id'];
}
$data = new User();
$result = $data->getUserByID($id);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['updatePass'])){
	$updatePass = $data->updatePass($id, $_POST);
}

$msg = Session::get("msg");

 if (isset($msg)){
     echo $msg;
 }

 Session::set('loginmsg', NULL);

?>
  <!--  Breadcrumb -->
  <div class="container">
      <div class="breadcrumb">
          <a href="index.php">Home</a>
          <a href="profile.php" class="active">Profile</a>
      </div>
  </div>
  <!--  // Breadcrumb -->
  <div class="container pull-right">
  <a href="index.php" class="btn btn-primary">Back </a>
  </div>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Change User Password</h3>
				</div >
				<?php 
					if(isset($updatePass)){
						echo $updatePass;
					}
				?>
				<div class="panel-body">
					<form action="" method="post">
						
						<div class="form-group">
							<label for="old_pass">Old Password:</label>
							<input type="password" class="form-control" name="old_pass" value="<?php echo $result->password; ?>">
						</div>
						<div class="form-group">
							<label for="new_pass">New Password:</label>
							<input type="password" class="form-control" name="new_pass"> 
						</div>
						
						<button class="btn btn-primary pull-right" type="submit" name="updatePass"> Update Password </button>
						      

							
					</form>

				</div>
			</div>
		</div>
	</div>
</div>  <!-- // login container ended here -->





<?php  require_once 'inc/footer.php'; ?>